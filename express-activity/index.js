const express = require("express");

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let users = [];

app.post("/register", (req, res) => {
	console.log(req.body);

	if(
        req.body.firstname !== '' && 
        req.body.lastname !== '' && 
        req.body.username !== '' && 
        req.body.password !== ''
    ){
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input ALL required information.")
	}
});

app.post("/login", (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    res.send(`Welcome ${username}! You are successfully logged in!`);
  });

app.listen(port, () => console.log(`Server running at port ${port}`))