fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(`The post item's title is "${json.title}" and its body is ${json.body}`));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Title',
		body: 'This is the updated body of this post item.'
	}),	
})
.then((response) => response.json())
.then((json) => console.log(json));
